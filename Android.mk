#
# Automatically generated file. DO NOT MODIFY
#

LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),salami)

$(call add-radio-file-sha1-checked,radio/abl.img,5da407bcab017647d9bb7bdc5c539764eaeadb68)
$(call add-radio-file-sha1-checked,radio/aop.img,56a5b81c8e3c901568c2e7f3d8592a3ffa45188f)
$(call add-radio-file-sha1-checked,radio/aop_config.img,c29639008ec3a12c2eedd49db23e2740c1379fca)
$(call add-radio-file-sha1-checked,radio/bluetooth.img,59a6404ffe596d054de05ceb74ea86c8ed91b318)
$(call add-radio-file-sha1-checked,radio/cpucp.img,ae1dadaae5a68aa1bb870610f60038aea3d32452)
$(call add-radio-file-sha1-checked,radio/devcfg.img,b3be6f0cec2dca5efbbcfdd1fafefe7dbf933495)
$(call add-radio-file-sha1-checked,radio/dsp.img,28885c51ced35d72cb8f10a1e250fe7a8c427bab)
$(call add-radio-file-sha1-checked,radio/engineering_cdt.img,13f5f33df779408b04cdfb3d78dd1ebbd3afb7b4)
$(call add-radio-file-sha1-checked,radio/featenabler.img,39232bfe2212835d7b7314082fcc49f822ec66de)
$(call add-radio-file-sha1-checked,radio/hyp.img,59124d14a067224a78df43a74e8727c8bb4f514c)
$(call add-radio-file-sha1-checked,radio/imagefv.img,e0b3baac12fd60304601ba9a587ee0e359231483)
$(call add-radio-file-sha1-checked,radio/keymaster.img,6ef5c25aae882ce377f1e8fc7c70d8141c0d2113)
$(call add-radio-file-sha1-checked,radio/modem.img,83f6d3b2a9ec7f650d4bd6689bd4f94233e4182e)
$(call add-radio-file-sha1-checked,radio/oplus_sec.img,1c584c7b98f32a7ccceebbad7564e3bc0944ccb0)
$(call add-radio-file-sha1-checked,radio/oplusstanvbk.img,0b7104bcd583756466eea90ca743c2c3c09c8ea5)
$(call add-radio-file-sha1-checked,radio/qupfw.img,e9ea57d4b89a3fda088915544e2f60629f34f458)
$(call add-radio-file-sha1-checked,radio/shrm.img,b76c2697021d9fb4573bc3f5a9192af8384cce67)
$(call add-radio-file-sha1-checked,radio/splash.img,8877705066051214763727acd252d62aba94aa5e)
$(call add-radio-file-sha1-checked,radio/tz.img,c63a459469b802e569266088f3e6c3fb81e16894)
$(call add-radio-file-sha1-checked,radio/uefi.img,1b670d970bf2eefec69ed16de89a3c32adda8d42)
$(call add-radio-file-sha1-checked,radio/uefisecapp.img,17d243d23a944484ad60369a47f68eab858e0b31)
$(call add-radio-file-sha1-checked,radio/xbl.img,a5a36952d44b01f101571c6e449ef1b5217b97a1)
$(call add-radio-file-sha1-checked,radio/xbl_config.img,451c8a0ce4d1c112f26d2512203cf5a6f938a2c7)
$(call add-radio-file-sha1-checked,radio/xbl_ramdump.img,ddb51a578f8f462202e6554fd14cfc9c603baae7)

endif
